const HtmlWebpackPlugin = require('html-webpack-plugin');



module.exports = {

  /**
   * Output configuration. Produces bundles based on entry point name
   *
   * @see  ./config/entries.config.js
   * @type {Object}
   */
  output: {

    path: `${__dirname}/dist`,
    filename: '[name].js',

  },



  /**
   * Entry configuration. Webpack will only process resources which are linked
   * to the entry point via import.
   *
   * @see ./config/entries.config.js
   * @type {Object}
   */
  entry: {
    ex001: './examples/001/index.js',
    ex002: './examples/002/index.js',
    ex003: './examples/003/index.js',
    ex004: './examples/004/index.js',
    ex005: './examples/005/index.js',
    ex006: './examples/006/index.js',
  },



  /**
   * Source map configuration. Mainly relates to entry file source mapping
   *
   * @type {String}
   */
  devtool: 'source-map',



  /**
   * Webpack module configuration. Currently only defines style bundling steps
   * for configured entries.
   *
   * @see  ./config/entries.config.js
   * @type {Object}
   * @property {Array} rules - defines build flow of items that are matched by
   *   item's `test` RegExp.
   */
  module: {

    rules: [

      {
        test: /\.js$/,
        exclude: [ /node_modules/, /Scripts/ ],
        loaders: [ 'babel-loader', 'eslint-loader' ],
      },

      {
        test: /\.html$/,
        loaders: [ 'raw-loader', 'html-minify-loader' ],
      },

    ],

  },



  devServer: { contentBase: './dist' },



  /**
   * Plugins configuration. Currently only stylelint and extract plugins are
   * used and configured.
   *
   * @type {Array}
   */
  plugins: [

    new HtmlWebpackPlugin({
      chunks: ['ex001'],
      filename: '001.html',
      title: 'Example 001: template imports',
      template: './examples/001/index.html',
    }),

    new HtmlWebpackPlugin({
      chunks: ['ex002'],
      filename: '002.html',
      title: 'Example 002: angularjs-annotate',
      template: './examples/002/index.html',
    }),

    new HtmlWebpackPlugin({
      chunks: ['ex003'],
      filename: '003.html',
      template: './examples/003/index.html',
    }),

    new HtmlWebpackPlugin({
      chunks: ['ex004'],
      filename: '004.html',
      template: './examples/004/index.html',
    }),

    new HtmlWebpackPlugin({
      chunks: ['ex005'],
      filename: '005.html',
      template: './examples/005/index.html',
    }),

    new HtmlWebpackPlugin({
      chunks: ['ex006'],
      filename: '006.html',
      template: './examples/006/index.html',
    }),

  ],

};
