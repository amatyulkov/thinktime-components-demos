import template from './comments-section.component.html';



class CommentsSectionController {

  constructor ($log, $timeout, $q) {

    'ngInject';

    this.$log = $log;
    this.$timeout = $timeout;
    this.$q = $q;

  }



  $onInit () {

    this.$log.debug('Comments Section Component initialized');

    this.comments = [];

  }



  onCommentSubmitted (comment) {

    this.$log.debug('3. Executing bound callback `on-comment-submitted`');

    const timestamp = new Date();
    const payload = { comment, timestamp };

    return this.trySave().then(
      () => this.onCommentSaved(payload),
      () => this.onCommentSaveFailed(payload),
    );

  }



  getRandomDelay () {

    const FLOOR = 250;
    const CEIL = 1000;

    return (Math.random() * (CEIL - FLOOR)) + FLOOR;

  }



  trySave () {

    const success = this.isSaveSuccessful();
    const httpDelay = this.getRandomDelay();
    const deferred = this.$q.defer();

    this.$timeout(() => {

      if (success) {

        this.$log.debug('4A. Bound callback returns a resolved promise');

        return deferred.resolve();

      }

      this.$log.debug('4B. Bound callback returns a rejected promise');

      return deferred.reject();

    }, httpDelay);

    return deferred.promise;

  }



  isSaveSuccessful () {

    const ERROR_RATE = 0.25;
    const isSuccessful = Math.random() > ERROR_RATE;

    this.$log.debug(isSuccessful ? 'Will pass' : 'Will fail');

    return isSuccessful;

  }



  onCommentSaved (comment) {

    this.$log.debug('5A. Success callback in parent controller');

    this.comments = [ comment, ...this.comments ];

    return this.comments;

  }



  onCommentSaveFailed (comment) {

    this.$log.debug('5B. Error callback in parent controller');
    this.$log.error(`Failed to save comment ${comment}`);

    return this.$q.reject();

  }

}



export const CommentsSectionComponent = {

  template,
  controller: CommentsSectionController,

};
