import angular from 'angular';

import { CommentsFormComponent } from './comments-form.component';



export const CommentsFormModule = angular
  .module('tt.comments.form', [])
  .component('ttCommentsForm', CommentsFormComponent)
  .name;
