import template from './comments-form.component.html';



class CommentsFormController {

  constructor ($log) {

    'ngInject';

    this.$log = $log;

  }



  $onInit () {

    this.$log.debug('Comments Form Component initialized');

    this.comment = '';
    this.lastSaveFailed = false;

  }



  onSubmit ($event, comment) {

    $event.preventDefault();

    if (comment) {

      this.$log.debug('1. Comment submitted');
      this.$log.debug('2. Calling bound callback `on-comment-submitted`');

      this.onCommentSubmitted({ comment }).then(
        () => this.onCommentSaved(),
        () => this.onCommentSaveFailed(),
      );

    }

  }



  onCommentSaved () {

    this.$log.debug('6A. Success callback in child controller');
    this.comment = '';
    this.lastSaveFailed = false;

  }



  onCommentSaveFailed () {

    this.$log.debug('6B. Error callback in child controller');
    this.lastSaveFailed = true;

  }

}



export const CommentsFormComponent = {

  template,
  controller: CommentsFormController,

  bindings: { onCommentSubmitted: '&' },

};
