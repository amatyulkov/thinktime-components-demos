import template from './comments-list.component.html';



class CommentsListController {

  constructor ($log) {

    'ngInject';

    this.$log = $log;
    this.comments = [];

  }



  $onInit () {

    this.$log.debug('Comments List Component initialized');

  }



  $onChanges () {

    this.$log.debug('7A. Magic! Comments list updated, rendering');

  }

}



export const CommentsListComponent = {

  template,
  controller: CommentsListController,

  bindings: { comments: '<' },

};
