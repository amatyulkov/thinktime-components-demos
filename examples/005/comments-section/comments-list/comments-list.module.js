import angular from 'angular';

import { CommentsListComponent } from './comments-list.component';



export const CommentsListModule = angular
  .module('tt.comments.list', [])
  .component('ttCommentsList', CommentsListComponent)
  .name;
