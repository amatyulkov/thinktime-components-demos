import angular from 'angular';

import { CommentsFormModule } from './comments-form';
import { CommentsListModule } from './comments-list';

import { CommentsSectionComponent } from './comments-section.component';



export const CommentsSectionModule = angular
  .module('tt.comments', [
    CommentsFormModule,
    CommentsListModule,
  ])
  .component('ttCommentsSection', CommentsSectionComponent)
  .name;
