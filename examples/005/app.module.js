import angular from 'angular';

import { CommentsSectionModule } from './comments-section';



export const AppModule = angular
  .module('tt', [CommentsSectionModule])
  .name;
