import angular from 'angular';

import { ParentModule } from './parent';



export const AppModule = angular
  .module('tt', [ParentModule])
  .name;
