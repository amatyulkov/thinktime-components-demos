import angular from 'angular';

import { NestedChildModule } from './nested-child';

import { ChildComponent } from './child.component';



export const ChildModule = angular
  .module('tt.lifecycle.child', [NestedChildModule])
  .component('ttChild', ChildComponent)
  .name;
