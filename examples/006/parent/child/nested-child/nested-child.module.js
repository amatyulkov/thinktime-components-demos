import angular from 'angular';

import { NestedChildComponent } from './nested-child.component';



export const NestedChildModule = angular
  .module('tt.lifecycle.nested-child', [])
  .component('ttNestedChild', NestedChildComponent)
  .name;
