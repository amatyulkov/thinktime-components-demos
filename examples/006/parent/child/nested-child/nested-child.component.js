import template from './nested-child.component.html';



class NestedChildController {

  constructor ($log) {

    'ngInject';

    this.$log = $log;

    this.$log.debug('Nested Child Component constructed');

  }



  $onInit () {

    this.$log.debug('Nested Child Component initialized');

  }



  $onChanges () {

    this.$log.debug('Nested Child Component updated');

  }



  $onDestroy () {

    this.$log.debug('Nested Child Component destroyed');

  }

}



export const NestedChildComponent = {

  template,
  controller: NestedChildController,

  bindings: { sub: '<' },

};
