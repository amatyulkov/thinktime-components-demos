import template from './child.component.html';



class ChildController {

  constructor ($log) {

    'ngInject';

    this.$log = $log;

    this.$log.debug('Child Component constructed');

  }



  $onInit () {

    this.$log.debug('Child Component initialized');

    this.id = this.child.id;
    this.subs = this.child.subs;

  }



  $onChanges () {

    this.$log.debug('Child Component updated');

  }



  $onDestroy () {

    this.$log.debug('Child Component destroyed');

  }



  remove (child) {

    return this.onRemove({ child });

  }



  up (subs) {

    this.subs = subs.map(sub => sub + 1);

    return this.subs;

  }

}



export const ChildComponent = {

  template,
  controller: ChildController,

  bindings: {
    child: '<',
    onRemove: '&',
  },

};
