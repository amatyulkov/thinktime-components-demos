import template from './parent.component.html';



class ParentController {

  constructor ($log) {

    'ngInject';

    this.$log = $log;

    this.children = [
      /* eslint-disable no-magic-numbers */
      { id: 'A', subs: [ 1, 2, 3 ] },
      { id: 'B', subs: [ 4, 5, 6 ] },
      { id: 'C', subs: [ 7, 8, 9 ] },
      /* eslint-enable no-magic-numbers */
    ];

    this.$log.debug('Parent component constructed');

  }



  $onInit () {

    this.$log.debug('Parent component initialized');

  }



  $onChanges () {

    this.$log.debug('Parent component updated');

  }



  $onDestroy () {

    this.$log.debug('Parent component destroyed');

  }



  onRemove (child) {

    const childIdx = this.children.indexOf(child);

    this.children = [
      ...this.children.slice(0, childIdx),
      ...this.children.slice(childIdx + 1, this.children.length),
    ];

  }

}



export const ParentComponent = {

  template,
  controller: ParentController,

};
