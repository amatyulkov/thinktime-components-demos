import angular from 'angular';

import { ChildModule } from './child';

import { ParentComponent } from './parent.component';



export const ParentModule = angular
  .module('tt.lifecycle.parent', [ChildModule])
  .component('ttParent', ParentComponent)
  .name;
