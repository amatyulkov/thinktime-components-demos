/**
 * App run
 *
 * @export
 * @param {any} $rootScope -
 * @returns {Object} -
 */
export function AppModuleRun ($rootScope) {

  const config = { locale: 'en' };

  return Object.assign($rootScope, config);

}
