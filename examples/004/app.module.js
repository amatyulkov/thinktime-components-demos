import angular from 'angular';

import { GoodParentModule } from './good-parent';
import { BadParentModule } from './bad-parent';

import { AppModuleRun } from './app.module.run';



export const AppModule = angular
  .module('tt', [
    GoodParentModule,
    BadParentModule,
  ])
  .run(AppModuleRun)
  .name;
