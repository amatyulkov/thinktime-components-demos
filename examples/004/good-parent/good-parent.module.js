import angular from 'angular';

import { GoodChildModule } from './good-child';

import { GoodParentComponent } from './good-parent.component';



export const GoodParentModule = angular
  .module('tt.good.parent', [GoodChildModule])
  .component('ttGoodParent', GoodParentComponent)
  .name;
