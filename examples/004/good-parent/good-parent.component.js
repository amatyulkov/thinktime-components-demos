import template from './good-parent.component.html';



class GoodParentController {

  constructor ($log) {

    this.$log = $log;

  }



  $onInit () {

    this.$log.debug('Good Parent Controller initialized');

    this.data = [
      /* eslint-disable no-magic-numbers */
      { id: 1, title: 'Apple' },
      { id: 2, title: 'Orange' },
      { id: 3, title: 'Peach' },
      /* eslint-enable no-magic-numbers */
    ];

    this.secret = 'good secret!';

  }

}


export const GoodParentComponent = {

  template,
  controller: GoodParentController,
  controllerAs: 'goodParent',

};
