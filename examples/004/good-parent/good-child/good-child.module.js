import angular from 'angular';

import { GoodChildComponent } from './good-child.component';



export const GoodChildModule = angular
  .module('tt.good.parent.child', [])
  .component('ttGoodChild', GoodChildComponent)
  .name;
