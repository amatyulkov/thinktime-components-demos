import template from './good-child.component.html';



class GoodChildController {

  constructor ($log) {

    'ngInject';

    this.$log = $log;

  }



  $onInit () {

    this.$log.debug('Good Child Component initialized');

  }



  debugSelf (data) {

    this.$log.debug(data);

  }

}



export const GoodChildComponent = {

  template,
  controller: GoodChildController,
  controllerAs: 'goodChild',

  bindings: { item: '<' },

};
