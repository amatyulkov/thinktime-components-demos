import template from './bad-parent.component.html';



class BadParentController {

  constructor ($log, $scope) {

    'ngInject';

    this.$log = $log;
    this.$scope = $scope;
    this.$scope.debugChild = data => this.debugChild(data);

  }



  $onInit () {

    this.$log.debug('Bad Parent Component initialized');

    this.$scope.data = [
      /* eslint-disable no-magic-numbers */
      { id: 1, title: 'Apple' },
      { id: 2, title: 'Orange' },
      { id: 3, title: 'Peach' },
      /* eslint-enable no-magic-numbers */
    ];

    this.$scope.secret = 'secret!';

  }



  debugChild (data) {

    // notice that is also prints out the $$hashKey
    this.$log.debug(data);

  }

}



export const BadParentComponent = {

  template,
  controller: BadParentController,

};
