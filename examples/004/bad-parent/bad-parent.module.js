import angular from 'angular';

import { BadParentComponent } from './bad-parent.component';



export const BadParentModule = angular
  .module('tt.bad.parent', [])
  .component('ttBadParent', BadParentComponent)
  .name;
