/**
 * Through webpack loaders, ThingTempalte is a string with contents of
 * thing.component.html. It is then used as `template` (no more `templateUrl`)
 * for the component
 */
import ThingTemplate from './thing.component.html';



/**
 * Component controller, more on that in 004.
 *
 * Classes, combined with arrow functions, allow for convenient usage of `this`
 * and unit-testing.
 *
 * @class ThingController
 */
class ThingController {

  constructor ($log) {

    'ngInject';

    this.$log = $log;

  }



  $onInit () {

    this.$log.debug('Thing Component initialized');

  }

}



/**
 * Component definition object. Must have a controller defined. Very similar
 * to directive definition object.
 *
 * https://docs.angularjs.org/guide/component
 */
export const ThingComponent = {

  template: ThingTemplate,
  controller: ThingController,

};
