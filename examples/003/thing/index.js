/**
 * Index file in component directory allows for shorter import syntax,
 * and allow to limit what is accessible from component directory.
 * In app.module.js, Thing component is imported as follows:
 *
 * import { ThingModule } from './thing'
 *
 * Without the index file, it would have been
 * import { ThingModule } from './thing/thing.module'; // .js may be omitted
 *
 * This is particularly useful in cases when a module is imported in a lot of
 * places throughout the app
 *
 * In general, index files only export the module name as long as component
 * directory structure is followed.
 */
export * from './thing.module';
