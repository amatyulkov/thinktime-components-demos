/**
 * Modules should be isolated and self-packed with all of the dependencies they
 * require. In Store Visits most of the *.module.js files import angular.
 *
 * Webpack keeps track of the imports, so for 50 occurrences of angular import,
 * the bundle only contains one copy. The benefit is that when a module has
 * explicit dependencies, it may be bundled separately, or moved, or removed.
 *
 * If a module (more likely - component) requires jQuery, or moment.js, or
 * something else vendor-provided to operate - such dependencies need to be
 * listed here
 *
 * ```
 * import $ from 'jquery'
 * ```
 */
import angular from 'angular';

/**
 * The `.module.js` file contains:
 *
 * 1. Angular module definition call
 * 2. Angular "thing" definition calls (.service, .directive, .component)
 *
 * In other words, the module file gives an overview of what's going to be
 * available when the module is imported.
 *
 * The actual definitions and implementations of angular "things" must be
 * contained externally to the module file. Here, ThingComponent is imported
 * from an external file, and then passed as component definition
 * to .component(...) call.
 *
 * Same would go for other stuff:
 *
 * ```
 * import { ThingTypesConstant } from './thing-types.constant';
 * import { ThingsService } from './things.service';
 * // etc
 * ```
 */
import { ThingComponent } from './thing.component';



/**
 * Now, all at once!
 *
 * ```
 * // /003/thing/thing.module.js
 * import { ThingComponent } from './thing.component';
 * export const ThingModule = angular.module(...).component(...).name;
 * // this chain of invocation returns a string (the module name), and it is
 * exported as ThingModule
 *
 * // /003/thing/index.js
 * export * from './thing.module';
 * // whatever is exported from module (the module name) - export from index.js
 *
 * // /003/app.module.js
 * import { ThingModule } from './thing'; // ThingModule is the module name
 * angular.module(..., [ThingModule]); // module name passed as dependency
 *
 * // /003/index.js
 * import './app.module.js';
 * ```
 *
 * /003/index.js is a webpack entry point. To bundle it, webpack recursively
 * goes through the imports and exports, builds a dependency tree, then flattens
 * it in a way that dependencies are now a matter of concat and load order.
 * As such, load order:
 *
 * - thing/thing.component.html
 * - thing/thing.component.js
 * - thing/thing.module.js
 * - thing/index.js
 * - app.module.js
 * - index.js -- entry
 *
 * And angular somewhere inbetween, but **before** it is required by
 * thing.module.js :)
 */
export const ThingModule = angular
  .module('namespace.nested-namespaces-if-any.thing', [])
  .component('namespaceThing', ThingComponent)
  .name;
