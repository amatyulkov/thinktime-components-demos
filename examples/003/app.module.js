import angular from 'angular';

import { ThingModule } from './thing';



export const AppModule = angular
  .module('namespace', [ThingModule])
  .name;
