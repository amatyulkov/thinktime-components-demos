import SomeComponentTemplate from './some.component.html';



export const SomeComponent = {

  template: SomeComponentTemplate,
  controller: [ '$log', function SomeComponentController ($log) {

    // 1. added indent
    // 2. quoted deps
    // 3. controller unreachable outside, can't import in unit tests

    $log.info('Controller initialized');

  } ],

};
