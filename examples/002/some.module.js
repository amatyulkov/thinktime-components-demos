import angular from 'angular';
import { SomeComponent } from './some.component';

export const SomeModule = angular
  .module('ex002', [])
  .component('someComponent', SomeComponent)
  .name;

angular.bootstrap(document.body, ['ex002']);
