import SomeComponentTemplate from './some.component.html';



class SomeComponentController {

  constructor ($log) {

    'ngInject'; // <-- profit!

    $log.info('Controller initialized');

  }

}



export const SomeComponent = {

  template: SomeComponentTemplate,
  controller: SomeComponentController,

};
