import SomeComponentTemplate from './some.component.html';



class SomeComponentController {

  constructor ($log) {

    $log.info('Controller initialized');

  }

}

// 1. classes, unlike functions, are not hoisted, so $inject is below class
// 2. still quoting dependencies
SomeComponentController.$inject = ['$log'];



export const SomeComponent = {

  template: SomeComponentTemplate,
  controller: SomeComponentController,

};
