import SomeComponentTemplate from './some.component.html';


SomeComponentController.$inject = ['$log'];

/**
 * 1. 0 indent,
 * 2. still quoted dependencies, now via $inject
 * 3. if required controller is now available for export: will need that
 *    in unit tests
 *
 * @param {any} $log -
 * @returns {void}
 */
function SomeComponentController ($log) {

  $log.info('Controller initialized');

}



export const SomeComponent = {

  template: SomeComponentTemplate,
  controller: SomeComponentController,

};
