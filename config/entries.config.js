const ExtractTextPlugin = require('extract-text-webpack-plugin');

const COMMON_BUNDLE_NAME = 'common';
const VENDORS_BUNDLE_NAME = 'vendors';

/**
 * This config array describes entry points. By default, webpack includes
 * compiled styles into the .js bundle, and extract-text-webpack-plugin is used
 * to produce the actual .css files.
 *
 * @type {Array}
 */
const CONFIGS = [

  new PageBundleConfig({
    type: 'common',
    bundle: COMMON_BUNDLE_NAME,
    path: '/src/common/index.js',
    stylesSrc: 'common.scss',
  }),

  new PageBundleConfig({
    type: 'vendors',
    bundle: VENDORS_BUNDLE_NAME,
    path: '/src/vendor-imports/index.js',
    stylesSrc: 'vendor-imports.scss',
  }),

  new PageBundleConfig({
    type: 'page',
    bundle: 'sv-results',
    path: '/src/areas/store-visits/results/index.js',
  }),

  new PageBundleConfig({
    type: 'page',
    bundle: 'sv-conduct',
    path: '/src/areas/store-visits/conduct/index.js',
  }),

];



const styleUtils = getStyleUtils(CONFIGS);

const stylePlugins = styleUtils.map(takeStylePlugins);
const styleRules = styleUtils.map(takeTypeRules);
const pages = takePageBundleNames(CONFIGS);
const common = COMMON_BUNDLE_NAME;
const vendors = VENDORS_BUNDLE_NAME;
const entry = getEntry(CONFIGS);



module.exports = {
  entry,
  stylePlugins,
  styleRules,
  pages,
  common,
  vendors,
};



/**
 * @constructor
 * @param {object} config - config.object
 * @param {string} config.type - entry type
 * @param {string} config.bundle - bundle component name, e.g. `store-visits`
 * @param {string} config.path - path to page entry
 * @param {string} config.stylesSrc - styles file name in case not a component
 */
function PageBundleConfig (config) {

  const bundle = config.bundle;
  const stylesSrc = config.stylesSrc || `${bundle}.component.scss`;
  const path = `.${config.path}`;
  const type = config.type || null;

  this.type = type;
  this.bundle = bundle;
  this.path = path;
  this.stylesSrc = new RegExp(stylesSrc.replace(/\./g, '\\.'));

}



/**
 * Produces the webpack entry object
 * @param {Array} configs - entry points config
 * @return {Object} - webpack entry object
 */
function getEntry (configs) {

  return configs.reduce(reduceToEntryObject, {});

}


/**
 * Adds bundle name as property and entry path as value
 *
 * @param  {Object} prev - available entry
 * @param  {Object} config - curent entry point config
 * @return {Object} - updated entry
 */
function reduceToEntryObject (prev, config) {

  const addition = {};

  addition[config.bundle] = config.path;

  return Object.assign(prev, addition);

}



/**
 * Gets styles utility objects
 *
 * @param  {Array} configs - entries config
 * @return {Array} - array of style util objects
 */
function getStyleUtils (configs) {

  return configs.map(getStyleUtil);

}



/**
 * Gets style utility objects. Creates a new extract plugin to take .css out of
 * generated .js bundles per entry, and sets the plugin instance to style rules
 * `use` property to execute. Rules are generated per entry in order to have
 * each bundle produce a separate .css file
 *
 * @param  {Object} config - entry config
 * @return {Object} - { plugin, rule }
 */
function getStyleUtil (config) {

  const loaders = [
    'css-loader?sourceMap',
    {
      loader: 'postcss-loader',
      options: { path: './config/postcss.config.js', sourceMap: true },
    },
    'sass-loader?sourceMap',
  ];

  const filename = `${config.bundle}.css`;
  const plugin = new ExtractTextPlugin({ filename });
  const rule = {
    test: config.stylesSrc,
    use: plugin.extract({ use: loaders }),
  };

  return { plugin, rule };

}


/**
 * Extract page bundle names from entry configs.
 *
 * @param {Array} configs - entries config
 * @return {Array} - array of bundle names
 */
function takePageBundleNames (configs) {

  return configs.filter(isPage).map(takeBundleName);

}



/**
 * Discerns page entry points
 *
 * @param  {Object} config - entry in question
 * @return {Boolean} - is page
 */
function isPage (config) {

  return config.type === 'page';

}


/**
 * Takes bundle name of a given entry config
 *
 * @param  {Object} config - entry in question
 * @return {string} - bundle name
 */
function takeBundleName (config) {

  return config.bundle;

}



/**
 * Takes plugin from a given style utility object
 *
 * @param  {Object} util - style utility object
 * @return {ExtractTextPlugin} - instance of ExtractTextPlugin
 */
function takeStylePlugins (util) {

  return util.plugin;

}



/**
 * Takes rule from a given style utility object
 *
 * @param  {Object} util - style utility object
 * @return {Object} - webpack module rule
 */
function takeTypeRules (util) {

  return util.rule;

}
