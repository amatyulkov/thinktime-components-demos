const rawLoader = require('raw-loader');



module.exports = {

  process: function process (src) {

    return rawLoader(src);

  },

};
