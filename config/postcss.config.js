const AUTOPREFIXER_CONFIG = require('./autoprefixer.config.js');



module.exports = {
  syntax: 'postcss-scss',
  plugins: {
    autoprefixer: AUTOPREFIXER_CONFIG,
    cssnano: {},
    'postcss-reporter': require('postcss-reporter')({ clearMessages: true }),
  }
}
